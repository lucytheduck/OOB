/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package duck;

import duck.interpreter.Brain;
import duck.interpreter.BrainObject;
import duck.utils.DebugInputStream;
import duck.utils.DebugOutputStream;
import duck.utils.DefaultInputStream;
import duck.utils.DefaultOutputStream;

import java.io.File;

public class Main {
    public static void main(String[] args){
        Brain.exec(new File("test.bc"));
        /*BrainObject mainbrain = Brain.loadObjectFromFile(new File("main.bf"));
        //mainbrain.out = new DebugOutputStream(System.out);
        //mainbrain.in = new DebugInputStream();
        mainbrain.out = new DefaultOutputStream(System.out);
        mainbrain.in = new DefaultInputStream();
        Brain.call_obj(mainbrain);*/
    }
}
