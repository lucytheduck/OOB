/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package duck.interpreter;

import duck.utils.DebugInputStream;
import duck.utils.DebugOutputStream;
import duck.utils.DefaultInputStream;
import duck.utils.DefaultOutputStream;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static duck.utils.Config.*;

public class BrainObject {
    public PrintStream out;
    public InputStream in;
    public int[] functionspace;
    public int[] dataspace;
    public int fpointer=0;
    public int dpointer=0;

    public BrainObject(){
        out = new DefaultOutputStream(System.out);
        in = new DefaultInputStream();
        //out = new DebugOutputStream(System.out);
        //in = new DebugInputStream();
        functionspace = new int[FUNCTION_SIZE];
        dataspace = new int[BUFFER_SIZE];
    }
}
