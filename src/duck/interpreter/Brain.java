/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package duck.interpreter;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

import static duck.utils.Config.*;

public class Brain {

    public final static int NEXT = 1;
    public final static int BACK = 2;
    public final static int INC = 3;
    public final static int DEC= 4;
    public final static int OUT= 5;
    public final static int IN = 6;
    public final static int JUMP_HEAD= 7;
    public final static int JUMP_TAIL= 8;
    public final static int CREATE_OBJECT = 9;
    public final static int DELETE_OBJECT = 10;
    public final static int CALL_OBJECT = 11;
    public final static int FORK_OBJECT = 12;
    public final static int KILL_OBJECT = 13;
    public final static int SET = 14;
    public final static int SYSCALL = 15;
    public final static int CONNECT = 16;
    public final static int DISCONNECT = 17;

    public final static HashMap<Integer, BrainObject> objects = new HashMap<>();
    public final static HashMap<BrainObject, Thread> object_threads = new HashMap<>();

    public static void create_object(int name, int newname){
        BrainObject ori = objects.get(name);
        BrainObject obj = new BrainObject();
        obj.functionspace = ori.functionspace;
        objects.put(newname, obj);
    }
    public static void kill_obj(BrainObject obj){
        Thread th = object_threads.get(obj);
        th.interrupt();
        objects.remove(obj);
        object_threads.remove(obj);
    }
    public static void call_obj(int name){
        call_obj(objects.get(name));
    }
    public static void call_obj(BrainObject obj){
        interprete(obj);
    }
    public static void fork_obj(int name){
        fork_obj(objects.get(name));
    }
    public static void fork_obj(@NotNull BrainObject obj){
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                interprete(obj);
            }
        });
        object_threads.put(obj, th);
        th.start();
    }
    public static void interprete(BrainObject obj){
        try {
            BrainObject currentObject = obj;
            int n, o;
            for (obj.fpointer = 0; obj.fpointer < BUFFER_SIZE; ) {
                switch (obj.functionspace[obj.fpointer]){
                    case NEXT:
                        currentObject.dpointer++;
                        break;
                    case BACK:
                        currentObject.dpointer--;
                        break;
                    case INC:
                        currentObject.dataspace[currentObject.dpointer]++;
                        break;
                    case DEC:
                        currentObject.dataspace[currentObject.dpointer]--;
                        break;
                    case OUT:
                        currentObject.out.print(currentObject.dataspace[currentObject.dpointer]);
                        break;
                    case IN:
                        currentObject.dataspace[currentObject.dpointer] = currentObject.in.read();
                        break;
                    case JUMP_HEAD:
                        //System.out.println("JUMP_HEAD");
                        if(obj.dataspace[obj.dpointer]==0){
                            //System.out.println("is 0");
                            int mark = obj.functionspace[++obj.fpointer];
                            obj.fpointer = getMarkTail(mark, obj)-1;
                        }
                        break;
                    case JUMP_TAIL:
                        //System.out.println("JUMP_TAIL");
                        if(obj.dataspace[obj.dpointer]!=0){
                            //System.out.println("is not 0");
                            int mark = obj.functionspace[obj.fpointer+1];
                            obj.fpointer = getMarkHead(mark, obj)-1;
                        }
                        break;
                    case CREATE_OBJECT:
                        o = obj.functionspace[++obj.fpointer];
                        n = obj.functionspace[++obj.fpointer];
                        create_object(o, n);
                        break;
                    case CALL_OBJECT:
                        o = obj.functionspace[++obj.fpointer];
                        call_obj(o);
                        break;
                    case FORK_OBJECT:
                        o = obj.functionspace[++obj.fpointer];
                        fork_obj(o);
                        break;
                    case CONNECT:
                        o = obj.functionspace[++obj.fpointer];
                        currentObject = objects.get(o);
                        break;
                    case DISCONNECT:
                        currentObject = obj;
                        break;
                    case SET:
                        n = obj.functionspace[++obj.fpointer];
                        currentObject.dataspace[currentObject.dpointer] = n;
                        break;
                }
                obj.fpointer++;
            }
        } catch (Exception e){
            e.printStackTrace();
            System.err.println("in cell "+obj.fpointer+" in object "+obj);
            System.exit(-1);
            return;
        } catch (StackOverflowError e){
            System.err.println("stackoverflow!");
            return;
        } catch (OutOfMemoryError e){
            System.err.println("out of memory!");
            System.exit(-1);
            return;
        }
    }
    private static int getMarkTail(int mark, BrainObject obj) {
        for(int i=0;i<BUFFER_SIZE;i++){
            if(obj.functionspace[i]==JUMP_TAIL){
                if(obj.functionspace[i+1]==mark){
                    return i+2;
                }
            }
        }
        return -1;
    }
    private static int getMarkHead(int mark, BrainObject obj) {
        for(int i=0;i<BUFFER_SIZE;i++){
            if(obj.functionspace[i]==JUMP_HEAD){
                if(obj.functionspace[i+1]==mark){
                    return i+2;
                }
            }
        }
        return -1;
    }
    public static void loadObject(BrainObject obj, int name){
        objects.put(name, obj);
    }
    public static BrainObject loadObjectFromFile(File file){
        BrainObject obj = new BrainObject();
        int co =0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            int i=0;
            int jpointer = 0;
            int _jpointer = 0;
            ArrayList<Integer> precode = new ArrayList<>();
            Stack<Integer> jumps = new Stack<>();
            do {
                line = br.readLine();
                String s;
                if(line==null)break;
                for(co = 0;co<line.length();co++) {
                    char c = line.charAt(co);
                    switch (c) {
                        case '>':
                            precode.add(NEXT);
                            break;
                        case '<':
                            precode.add(BACK);
                            break;
                        case '+':
                            precode.add(INC);
                            break;
                        case '-':
                            precode.add(DEC);
                            break;
                        case '.':
                            precode.add(OUT);
                            break;
                        case ',':
                            precode.add(IN);
                            break;
                        case '[':
                            precode.add(JUMP_HEAD);
                            /*precode.add(-1);
                            jumps.push(++_jpointer);
                            _jpointer++;*/
                            precode.add(jpointer);
                            jumps.push(jpointer++);
                            break;
                        case ']':
                            precode.add(JUMP_TAIL);
                            precode.add(jumps.pop());
                            /*int j = jumps.pop();
                            precode.add(j-1);
                            //precode.set(j, _jpointer++);*/
                            break;
                        case '/':
                            precode.add(CREATE_OBJECT);
                            s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else if(t==' ')break;
                            }
                            precode.add(Integer.parseInt(s));
                            s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else break;
                            }
                            co--;
                            precode.add(Integer.parseInt(s));
                            break;
                        case '!':
                            precode.add(CALL_OBJECT);
                            s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else break;
                            }
                            co--;
                            precode.add(Integer.parseInt(s));
                            break;
                        case '~':
                            precode.add(FORK_OBJECT);
                            s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else break;
                            }
                            co--;
                            precode.add(Integer.parseInt(s));
                            break;
                        case '=':
                            precode.add(SET);
                            s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else break;
                            }
                            co--;
                            precode.add(Integer.parseInt(s));
                            break;
                        case '?':
                            precode.add(SYSCALL);
                            s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else break;
                            }
                            co--;
                            precode.add(Integer.parseInt(s));
                            break;
                        case '@':
                            if(line.charAt(co+1)=='@'){
                                precode.add(DISCONNECT);
                                co++;
                                break;
                            } else {
                                precode.add(CONNECT);
                                s = "";
                                for (co++; co < line.length(); co++) {
                                    char t = line.charAt(co);
                                    if (isNumber(t)) s += t;
                                    else break;
                                }
                                co--;
                                precode.add(Integer.parseInt(s));
                                break;
                            }
                        default:
                            System.err.println("unknown instruction <" + c + "> "+co);
                            break;
                    }
                    /**
                     *
                     * 1
                     * 2
                     * 2
                     * 3
                     * 3
                     * 1
                     *
                     *
                     *
                     *
                     *
                     */
                    /*

                            String s = "";
                            for(co++;co<line.length();co++){
                                char t = line.charAt(co);
                                if(isNumber(t))s+=t;
                                else break;
                            }
                            precode.add(Integer.parseInt(s));

                     */
                }
                _jpointer++;
            } while (line!=null);
            int l=0;
            for(int j : precode){
                //System.out.println(j);
                obj.functionspace[l++] = j;
            }
            System.out.print("\n\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return obj;
    }

    private static boolean isNumber(String s) {
        try {
            int i = Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    private static boolean isNumber(char c){
        try {
            String s = "" + c;
            int i = Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    public static void exec(File file) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            int main=0;
            do {
                line = br.readLine();
                if(line!=null){
                    if(line.startsWith("@obj ")){
                        BrainObject o = loadObjectFromFile(new File(line.split(" ")[1]));
                        System.out.println("load object <"+line.split(" ")[1]+"> to <"+line.split(" ")[2]+">");
                        loadObject(o, Integer.parseInt(line.split(" ")[2]));
                    } else if(line.startsWith("@main ")){
                        main = Integer.parseInt(line.split(" ")[1]);
                    }
                }
            } while(line!=null);
            System.out.println("call "+main);
            call_obj(main);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }
}
