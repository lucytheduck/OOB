/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package duck.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class DefaultInputStream extends InputStream{
    Scanner scanner=null;
    @Override
    public int read() throws IOException {
        if(scanner==null)scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
