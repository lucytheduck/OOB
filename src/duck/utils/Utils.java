/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package duck.utils;


import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Utils {
    public static float bytes2float(byte[] b){
        return ByteBuffer.wrap(b).getFloat();
    }
    public static byte[] float2bytes(float f){
        return ByteBuffer.allocate(4).putFloat(f).array();
    }
    public static byte[] int2bytes(int i){
        return ByteBuffer.allocate(4).putInt(i).array();
    }
    public static int bytes2int(byte[] b){
        return ByteBuffer.wrap(b).getInt();
    }
    public InputStream fileToInputstream(File file){
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            System.exit(-1);
            //java r u fuckin stupid?!
            return null;
        }
    }
    public static String loadResource(String fileName) throws Exception {
        String result;
        /*try (InputStream in = Utils.class.getClass().getResourceAsStream(fileName);
                Scanner scanner = new Scanner(in, "UTF-8")) {
            result = scanner.useDelimiter("\\A").next();
        }*/
        //TODO: edit
        String s = "";
        //fileName.replace('/', '\\');
        //System.out.println(fileName);
        for(String t : Files.readAllLines(Paths.get(fileName)))s+=t+"\n";
        return s;
    }

    public static List<String> readAllLines(String fileName) throws Exception {
        List<String> list = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(Utils.class.getClass().getResourceAsStream(fileName)))) {
            String line;
            while ((line = br.readLine()) != null) {
                list.add(line);
            }
        }
        return list;
    }

    public static int[] listIntToArray(List<Integer> list) {
        int[] result = list.stream().mapToInt((Integer v) -> v).toArray();
        return result;
    }

    public static float[] listToArray(List<Float> list) {
        int size = list != null ? list.size() : 0;
        float[] floatArr = new float[size];
        for (int i = 0; i < size; i++) {
            floatArr[i] = list.get(i);
        }
        return floatArr;
    }

    public static boolean existsResourceFile(String fileName) {
        boolean result;
        try (InputStream is = Utils.class.getResourceAsStream(fileName)) {
            result = is != null;
        } catch (Exception excp) {
            result = false;
        }
        return result;
    }

    public static String fixIntToString(int i, int l, char place) {
        String t = Integer.toString(i);
        if(t.length()<=l){
            for(int c=t.length();c<l;c++)t = place+t;
        }
        return t;
    }
}
