/**
 /* This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 3.0, as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package duck.utils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lucy on 12.03.17.
 */
public abstract class TimeKeeper {
    static HashMap<String, Long> captures = new HashMap<>();
    static ArrayList<Thread> threads = new ArrayList<>();
    static HashMap<String, Long> perSec = new HashMap<>();
    static HashMap<String, Long> fps = new HashMap<>();
    static HashMap<String, Long> buffer = new HashMap<>();
    private static HashMap<String, Long> innerBuffer = new HashMap<>();
    public static void shutdown(){
        for(Thread th : threads)th.stop();
    }
    public static void mark(String s) {
        buffer.put(s, System.currentTimeMillis());
        try {perSec.put(s, perSec.get(s)+1); } catch (NullPointerException e){}
    }
    public static void print(String s){
        long t = System.currentTimeMillis()-buffer.get(s);
        if(t!=0)System.out.println(s+" = "+t+" ms");
    }
    private static void innerMark(String s) {
        innerBuffer.put(s, System.currentTimeMillis());
    }
    private static long innerGet(String s){
        return System.currentTimeMillis()-innerBuffer.get(s);
    }
    public static void startRecPerSec(String s){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("start recording perSec for \""+s+"\"");
                int c = 0;
                perSec.put(s, 0L);
                while(true){
                    fps.put(s, perSec.get(s));
                    perSec.put(s, 0L);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        });
        if ( System.getProperty("os.name").contains("Mac") ) thread.run();
        else thread.start();
        threads.add(thread);
    }
    public static void getPerSec(String s){
        if(fps.get(s) != 0)System.out.println(s+":"+fps.get(s)+" times per second");
    }

    public static void beginCapture(String s){
        captures.put(s, 0L);
    }
    public static void capture(String s){
        innerMark(s);
    }
    public static void pause(String s){
        captures.put(s, captures.get(s)+innerGet(s));
    }
    public static void endCapture(String s){
        System.out.println(s+" (captured) = "+captures.get(s));
    }

    public static long get(String ping) {
        return System.currentTimeMillis()-buffer.get(ping);
    }
}